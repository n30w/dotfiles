# Dotfiles

These are my dotfiles. I use `chezmoi` to manage them. 

My default terminal is **`fish`**, and my package manager on MacOS is **`homebrew`**.
